package com.zipkin.zipkin;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.HashMap;

@FeignClient(name = "cotacao", url = "https://viacep.com.br/ws/")
public interface CepFeign {

    @GetMapping("/{cep}/json/")
    HashMap<String, Object> buscar(@PathVariable String cep);
}

