package com.zipkin.zipkin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class CepController {
    @Autowired
    private CepService cepService;

    @GetMapping("/{cep}")
    public HashMap<String, Object> buscar(@PathVariable String cep) {
        return cepService.buscar(cep);
    }


}
